const tablette = document.querySelector("#tablette");
const joueur = document.querySelector("#joueur");
const retry = document.querySelector("#retry");
const largeur = document.querySelector("#largeur");
const hauteur = document.querySelector("#hauteur");
const popup = document.querySelector("#popup");
let position = document.querySelector("#position").value.replace(' ', '').split(",");

const carre = document.createElement('td');
carre.classList.add("carre");

const cassure = document.createElement('td');
cassure.classList.add("cassure");
const cassureVerticale = cassure.cloneNode();
cassureVerticale.classList.add("vertical");
cassureVerticale.title = "Casser verticalement ici";
const cassureHorizontale = cassure.cloneNode();
cassureHorizontale.classList.add("horizontal");
cassureHorizontale.title = "Casser horizontalement ici";

const shuffleArray = array => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
  }

const histoire = "<div class='block'><h1>Le carré de chocolat maudit</h1><p>Il s'agit d'un jeu de stratégie à deux joueurs.</p><p>On joue sur une grille rectangulaire de dimensions quelconques.</p><p>Elle représente une tablette de chocolat dans laquelle on a substitué un carré de savon (représenté en bleu ici) à un carré de chocolat.</p><p><img src='./assets/exemple.png' /></p><p>Chaque joueur, à tour de rôle, doit casser la tablette en deux, en une seule fois, soit horizontalement soit verticalement, puis donner à l'autre joueur le reste de la tablette contenant le carré de savon.</p><p>Le perdant est celui qui se voit contraint de prendre – sinon de manger – le carré de savon !</p></div>";

function preventDefaults(e) {
	e.preventDefault();
	e.stopPropagation();
}

function casser(sens, indice, position){
  let l = Math.ceil(tablette.children[0].children.length / 2);
  let h = Math.ceil(tablette.children.length / 2);

  joueur.innerHTML = (parseInt(joueur.innerHTML) % 2) + 1;

  if (sens == 'V'){
    if (indice < position[0])
      generer(l - (indice + 1), h, [position[0] - (indice + 1), position[1]]);
    else 
      generer(indice + 1, h, position);
  }
  else {
    if (indice < position[1])
      generer(l, h - (indice + 1), [position[0], position[1] - (indice + 1)]);
    else
      generer(l, indice + 1, position);
  }
}

function generer(largeur, hauteur, position){
  let x = position[0];
  let y = position[1];

  cassureVerticale.rowSpan = 2 * hauteur - 1;
  cassureHorizontale.colSpan = 2 * largeur - 1;

  tablette.innerHTML = "";

  let tr = document.createElement('tr');
  tablette.appendChild(tr);
  for (let i = 0; i < largeur; i++){
    tr.appendChild(carre.cloneNode());
    if (i < largeur - 1){
      let c = cassureVerticale.cloneNode();
      c.addEventListener('click', function (){
        casser('V', i, position);
      });
      tr.appendChild(c);
    }
  }
  
  for (let i = 0; i < hauteur - 1; i++){
    tr = document.createElement('tr');
    let c = cassureHorizontale.cloneNode();
    c.addEventListener('click', function (){
      casser('H', i, position);
    });
    tr.appendChild(c);
    tablette.appendChild(tr);

    tr = document.createElement('tr');
    for (j = 0; j < largeur; j++){
      tr.appendChild(carre.cloneNode());
    }
    tablette.appendChild(tr);
  }
  tablette.children[y * 2].querySelectorAll(".carre")[x].classList.add("savon");

  if (tablette.children.length == 1 && tablette.children[0].children.length == 1){
    document.querySelector("#popup-message").innerHTML = "<h1>Le joueur " + joueur.innerHTML + " a perdu !!!</h1>";
    popup.style.display = "flex";
  }
}

retry.addEventListener('click', () => {
  joueur.innerHTML = "1";
  generer(largeur.value, hauteur.value, position);
});

popup.addEventListener('click', function(){
    this.style.display = "none";
});

document.querySelector("#history").addEventListener('click', function(){
    document.querySelector("#popup-message").innerHTML = histoire;
    popup.style.display = "flex";
});

largeur.addEventListener('change', () => {
  joueur.innerHTML = "1";
  generer(largeur.value, hauteur.value, position);
});
hauteur.addEventListener('change', () => {
  joueur.innerHTML = "1";
  generer(largeur.value, hauteur.value, position);
});

document.querySelector("#position").addEventListener('change', function() {
  position = this.value.replace(' ', '').split(",");
  joueur.innerHTML = "1";
  generer(largeur.value, hauteur.value, position);
});

generer(largeur.value, hauteur.value, position);